#include <stdio.h>
#include "swap.h"

void selection_sort(int s[], int n)
{
    int j = 0, min = 0;

    for (int i = 0; i < n; ++i) {
        min = i;
        for (j = i+1; j < n; ++j) {
            if (s[j] < s[min]) {
                min = j;
            }
        }
        swap(&s[i],&s[min]);
    }
}

int main()
{
    int test[5];
    test[0] = 5;
    test[1] = 3;
    test[2] = 7;
    test[3] = 1;
    test[4] = 0;

    selection_sort(test, 5);

    for (int i = 0; i < 5; ++i) {
        printf("%d,", test[i]);
    }
    printf("\n");

    return 0;
}

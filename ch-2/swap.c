void swap(int *i, int *j)
{
    int tmp = *j;
    *j = *i;
    *i = tmp;
}

#include <stdio.h>
#include "swap.h"

void insertion_sort(int s[], int n)
{
    for (int i=1; i<n; ++i) {
        int j = i;
        while ((j>0) && (s[j] < s[j-1])) {
            swap(&s[j], &s[j-1]);
            j = j-1;
        }
    }
}

int main()
{
    int test[5];
    test[0] = 5;
    test[1] = 3;
    test[2] = 7;
    test[3] = 1;
    test[4] = 0;

    insertion_sort(test, 5);

    for (int i = 0; i < 5; ++i) {
        printf("%d,", test[i]);
    }
    printf("\n");

    return 0;
}
